#!/bin/bash

echo Patching $1
sed -i 's/shell.density 0.000000/shell.density 2698.899902/g' $1
sed -i 's/shell.heat_capacity 0.000000/shell.heat_capacity 903.000000/g' $1
sed -i 's/shell.thermal_conductivity 0.000000/shell.thermal_conductivity 237.000000/g' $1
sed -i 's/shell.thickness 0.000000/shell.thickness 0.010000/g' $1

sed -i 's/front.specular_reflectance 0.000000/front.specular_reflectance 0.200000/g' $1
sed -i 's/rear.specular_reflectance 0.000000/rear.specular_reflectance 0.200000/g' $1

sed -i 's/front.diffuse_reflectance 0.000000/front.diffuse_reflectance 0.100000/g' $1
sed -i 's/rear.diffuse_reflectance 0.000000/rear.diffuse_reflectance 0.100000/g' $1

sed -i 's/front.absorbance 0.000000/front.absorbance 0.700000/g' $1
sed -i 's/rear.absorbance 0.000000/rear.absorbance 0.700000/g' $1

sed -i 's/front.emissivity 0.000000/front.emissivity 0.900000/g' $1
sed -i 's/rear.emissivity 0.000000/rear.emissivity 0.900000/g' $1

