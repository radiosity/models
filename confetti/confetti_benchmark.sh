#!/bin/bash

rm -f benchmark.txt
touch benchmark.txt

for elem in 1000 10000 100000 1000000; do
  for rays in 100000 1000000 10000000 100000000; do
    echo "elem $elem rays $rays" >>benchmark.txt
    bin/controller ../models/confetti/confetti_${elem}.obj ../models/confetti/empty.task ${rays}>>benchmark.txt
done
done

